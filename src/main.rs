use std::env;
use std::error::Error;
use std::fs;
use cl_tool::process_text;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        eprintln!("Usage: {} <input> <input>", args[0]);
        eprintln!("input: String");
        eprintln!("mode: Upper or Lower");
        std::process::exit(1);
    }

    let input = &args[2];
    let mode = &args[1];

    let contents = if input.ends_with(".txt") {
        fs::read_to_string(input)?
    } else {
        input.clone()
    };

    let processed_contents = process_text(&contents, mode)?;

    println!("{}", processed_contents);

    Ok(())
}
