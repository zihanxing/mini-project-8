# IDS 721: Weekly Miniproject 8: Command-line Tool

## Simple Letter Case Transformation Tool

This Rust-based command-line tool facilitates text processing by converting it to either uppercase or lowercase. It accepts input directly as a string.

## Instructions

Follow these steps to set up and utilize the tool:

1. Begin by generating a new Cargo project using the command `cargo new <YOUR-PROJECT-NAME> --bin` (e.g., `cl-tool`).

2. Customize the `Cargo.toml` and `src/main.rs` files to align with your project's design and requirements. For example, you may create functions tailored to your specific functionality. (In this project, a simple function has been crafted to return the frequency of every character in the input string.)

3. Define the test cases within the `src/lib.rs` file.

4. To locally test the functionality of your project, navigate to its folder and execute `cargo test`.

## Test Results

![Screenshot of Test Results](test.png)

## Usage Example

![Screenshot of Usage](usage.png)
